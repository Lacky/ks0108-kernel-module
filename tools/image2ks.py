#!/usr/bin/env python3

from PIL import Image
import glob
import sys
import os
import os.path

def createLine(imageData, lineNumber):
    line = 0
    for i in range(8):
        index = (lineNumber%128 + int(lineNumber/128)*1024) + 128 * i
        line |= ((imageData[index]) & 1) << i

    return line & 255

def convert(path):
    inputImage = Image.open(path)
    imageBW = inputImage.convert('1')
    imageData = list(imageBW.getdata())
    imageConverted = []

    for i in range(1024):
        imageConverted.append(createLine(imageData, i))

    return bytes(imageConverted)

def getListOfFrames(dirPath):
    path = os.path.join(dirPath, "*.png")
    return glob.glob(path)

def getNewFilename(filename):
    formatExt = filename.rfind('.')
    return filename[:formatExt+1]+"ks0108"

def main(path):
    filesList = getListOfFrames(path)
    dir = os.path.split(filesList[0])[0]
    newDir = os.path.join(dir, "converted")
    try:
        os.mkdir(newDir)
    except FileExistsError:
        pass

    for pngFile in filesList:
        print("Converting {0}\n".format(pngFile))
        converted = convert(pngFile)
        filename = os.path.split(pngFile)[1]
        newFilename = getNewFilename(filename)
        f = open(os.path.join(newDir, newFilename), "wb")
        f.write(converted)
        f.close()

if __name__=="__main__":
    main(sys.argv[1])
