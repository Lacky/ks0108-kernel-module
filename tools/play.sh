#!/bin/bash

FRAMES_DIR=/home/pi/converted
FRAMES_LIST=`ls -v -d $FRAMES_DIR/*`

for f in $FRAMES_LIST
do
    echo "Sending $f"
    cat $f > /dev/lcd
    sleep $1
done
