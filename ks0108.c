#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h> 
#include <linux/fs.h> 
#include <linux/gpio.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <asm/uaccess.h>

#define  DEVICE_NAME "lcd"    
#define  CLASS_NAME "lcd"

#define INIT_STEP_GPIO_IS_VALID 0
#define INIT_STEP_GPIOS_REQUEST 1
#define INIT_STEP_GPIO_EXPORT 2
#define INIT_STEP_REGISTER_CHRDEV 3
#define INIT_STEP_CLASS_CREATE 4
#define INIT_STEP_DEVICE_CREATE 5
#define INIT_STEP_END 6

MODULE_LICENSE("GPL");            
MODULE_AUTHOR("Artur \"Lacky\" Łącki");
MODULE_DESCRIPTION("Driver for KS0108 display");  
MODULE_VERSION("0.1");       

#define FB_SIZE (128*8)
static u8 framebuffer[FB_SIZE] = {0};

static unsigned int pin_cs1 = 18;
static unsigned int pin_cs2 = 23;
static unsigned int pin_rs = 24;
static unsigned int pin_rw = 25;
static unsigned int pin_en = 12;
static unsigned int pin_db[] = {16, 20, 21, 4, 17, 27, 22, 10};
static bool sync_mode_en = false;

module_param(pin_cs1, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
module_param(pin_cs2, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
module_param(pin_rs, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
module_param(pin_rw, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
module_param(pin_en, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
module_param_array(pin_db, uint, NULL, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
module_param(sync_mode_en, bool, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);

MODULE_PARM_DESC(pin_cs1, "GPIO pin connected to CS1");
MODULE_PARM_DESC(pin_cs2, "GPIO pin connected to CS2");
MODULE_PARM_DESC(pin_rs, "GPIO pin connected to R/S (also called D/I)");
MODULE_PARM_DESC(pin_rw, "GPIO pin connected to R/W");
MODULE_PARM_DESC(pin_en, "GPIO pin connected to Enable");
MODULE_PARM_DESC(pin_db, "GPIO pins connected to DB (in order from DB0 to DB7)");
MODULE_PARM_DESC(sync_mode_en, "Synchronous transmission with display");


typedef struct command_s {
    bool rs;
    bool rw;
    u8 db;
} command_t;

#define DB_PINS_NUMBER ARRAY_SIZE(pin_db)
#define CONTROL_PINS_NUMBER 5
static struct gpio gpio_array[DB_PINS_NUMBER+CONTROL_PINS_NUMBER];
static struct class*  lcd_class  = NULL; 
static struct device* lcd_device = NULL;

static int major_number;

static int ks0108_open(struct inode *inodep, struct file *filep);
static int ks0108_release(struct inode *inodep, struct file *filep);
static ssize_t ks0108_read(struct file *filep, char *buffer, size_t len, loff_t *offset);
static ssize_t ks0108_write(struct file *filep, const char *buffer, size_t len, loff_t *offset);

static void init_gpio_struct(struct gpio* gpio_element, unsigned gpio, unsigned long flags,
                            const char* label);
static int uninit(int step);
static int init_gpios(void);
static int init_device(void);
static int ks0108_open(struct inode *inodep, struct file *filep);
static int ks0108_release(struct inode *inodep, struct file *filep);
static void wait_for_lcd(void);
static int select_cs(unsigned int pin_cs);
static int set_output_mode(void);
static int set_input_mode(void);
static bool bit_read(u8 data, u8 bit_number);
static void write_to_db(u8 db_data);
static u8 get_db_data(void);
static bool lcd_is_busy(u8 data);
static u8 exec_lcd_cmd(command_t *cmd);
static void display_turn(bool enable);
static void set_addr_y(u8 y);
static void set_page_x(u8 x);
static void write_display_data(u8 data);
static u8 status_read(void);
static void write_screen_data(u8 data[]);
static int init_lcd(void);

 
static struct file_operations fops =
{
   .open = ks0108_open,
   .read = ks0108_read,
   .write = ks0108_write,
   .release = ks0108_release,
};

static void init_gpio_struct(struct gpio* gpio_element, unsigned gpio, unsigned long flags,
                            const char* label)
{
    gpio_element->gpio = gpio;
    gpio_element->flags = flags;
    gpio_element->label = label;
}


static int uninit(int step)
{
    switch(step)
    {
        case INIT_STEP_END:
            device_destroy(lcd_class, MKDEV(major_number, 0));
            step--;
        case INIT_STEP_DEVICE_CREATE:
            class_destroy(lcd_class);
            step--;
        case INIT_STEP_CLASS_CREATE:
            unregister_chrdev(major_number, DEVICE_NAME);
            step--;
        case INIT_STEP_REGISTER_CHRDEV:
            step--;
        case INIT_STEP_GPIO_EXPORT:
            for (int i=0; i<ARRAY_SIZE(gpio_array); i++)
            {
                unsigned gpio_pin = gpio_array[i].gpio;
                gpio_unexport(gpio_pin);
            }
            step--;
        case INIT_STEP_GPIOS_REQUEST:
            gpio_free_array(gpio_array, ARRAY_SIZE(gpio_array));
            step--;
        case INIT_STEP_GPIO_IS_VALID:
            ;
        default:
            ;
    }
    return -ENODEV;
}

static int init_gpios(void)
{    
    init_gpio_struct(&(gpio_array[0]), pin_cs1, GPIOF_OUT_INIT_HIGH, "CS1");
    init_gpio_struct(&(gpio_array[1]), pin_cs2, GPIOF_OUT_INIT_HIGH, "CS2");
    init_gpio_struct(&(gpio_array[2]), pin_rs, GPIOF_OUT_INIT_LOW, "RS");
    init_gpio_struct(&(gpio_array[3]), pin_rw, GPIOF_OUT_INIT_LOW, "RW");
    init_gpio_struct(&(gpio_array[4]), pin_en, GPIOF_OUT_INIT_LOW, "EN");
    init_gpio_struct(&(gpio_array[5]), pin_db[0], GPIOF_OUT_INIT_LOW, "DB0");
    init_gpio_struct(&(gpio_array[6]), pin_db[1], GPIOF_OUT_INIT_LOW, "DB1");
    init_gpio_struct(&(gpio_array[7]), pin_db[2], GPIOF_OUT_INIT_LOW, "DB2");
    init_gpio_struct(&(gpio_array[8]), pin_db[3], GPIOF_OUT_INIT_LOW, "DB3");
    init_gpio_struct(&(gpio_array[9]), pin_db[4], GPIOF_OUT_INIT_LOW, "DB4");
    init_gpio_struct(&(gpio_array[10]),pin_db[5], GPIOF_OUT_INIT_LOW, "DB5");
    init_gpio_struct(&(gpio_array[11]),pin_db[6], GPIOF_OUT_INIT_LOW, "DB6");
    init_gpio_struct(&(gpio_array[12]),pin_db[7], GPIOF_OUT_INIT_LOW, "DB7");
    
    for (int i=0; i<ARRAY_SIZE(gpio_array); i++)
    {
        unsigned gpio_pin = gpio_array[i].gpio;        
        if (!gpio_is_valid(gpio_pin))
        {
            printk(KERN_ERR "[KS0108] %d is not a valid number of GPIO pin\n", gpio_pin);
            return uninit(INIT_STEP_GPIO_IS_VALID);            
        }
    }
    
    if (gpio_request_array((const struct gpio *)&gpio_array, ARRAY_SIZE(gpio_array)) != 0)
    {
        printk(KERN_ERR "[KS0108] Request for GPIOs has been rejected\n");
        return uninit(INIT_STEP_GPIOS_REQUEST);
    }
    
    for (int i=0; i<ARRAY_SIZE(gpio_array); i++)
    {
        unsigned gpio_pin = gpio_array[i].gpio;
        if (gpio_export(gpio_pin, true) != 0)
        {
            printk(KERN_ERR "[KS0108] Something went wrong during export pin %d\n", gpio_pin);
            return uninit(INIT_STEP_GPIO_EXPORT);
        }        
    }
    
    return 0;    
}

static int init_device(void)
{
    major_number = register_chrdev(0, DEVICE_NAME, &fops);
    if (major_number<0)
    {
        printk(KERN_ERR "[KS0108] Failed to register a major number\n");
        uninit(INIT_STEP_REGISTER_CHRDEV);
        return major_number;
    }
    
    lcd_class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(lcd_class))
    {
        printk(KERN_ERR "[KS0108] Failed to create a device class\n");
        uninit(INIT_STEP_CLASS_CREATE);
        return PTR_ERR(lcd_class);
    }
    
    lcd_device = device_create(lcd_class, NULL, MKDEV(major_number, 0), NULL, DEVICE_NAME);
    if (IS_ERR(lcd_device))
    {
        printk(KERN_ERR "[KS0108] Failed to create a device\n");
        uninit(INIT_STEP_DEVICE_CREATE);
        return PTR_ERR(lcd_device);
    }
    
    return 0;
}

static int ks0108_open(struct inode *inodep, struct file *filep)
{
    return 0;
}

static int ks0108_release(struct inode *inodep, struct file *filep)
{
    write_screen_data(framebuffer);
    return 0;
}

static ssize_t ks0108_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
    size_t data_to_read = ((len <= FB_SIZE) ? len : FB_SIZE);
    int error_count = 0;
    error_count = copy_to_user(buffer, framebuffer, data_to_read);

    if (error_count==0)
    {
       return data_to_read;  
    }
    else 
    {
       printk(KERN_WARNING "[KS0108] Failed to send %d characters to the user\n", error_count);
       return -EFAULT;
    }
}

static ssize_t ks0108_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
    size_t data_to_read = ((len>FB_SIZE) ? FB_SIZE : len);
    ssize_t read_len = copy_from_user(framebuffer, buffer, data_to_read);
    if (read_len != 0) printk(KERN_WARNING "[KS0108] %d bytes has not been copied\n", read_len);
    return data_to_read;
}

static void wait_for_lcd(void){
    u8 status;
    do {
        status_read(); //dummy read
        status = status_read();
    } while (lcd_is_busy(status));
}

static int select_cs(unsigned int pin_cs)
{
    if (pin_cs == pin_cs1)
    {
        gpio_set_value(pin_cs2, 1);
        gpio_set_value(pin_cs1, 0);
    }
    
    else if (pin_cs == pin_cs2)
    {
        gpio_set_value(pin_cs1, 1);
        gpio_set_value(pin_cs2, 0);
    }
    
    else
    {
        printk(KERN_ERR "[KS0108] Unknown CS pin %d\n", pin_cs);
        return -EFAULT;
    }
    
    udelay(10);
    return 0;
}

static int set_output_mode(void)
{
    for (int i=0; i<DB_PINS_NUMBER; i++)
    {
        int change_direction_status;
        change_direction_status = gpio_direction_output(pin_db[i], 0);
        if (change_direction_status != 0)
        {
            printk(KERN_ERR "[KS0108] Cannot set pin %d as output\n", pin_db[i]);
            return change_direction_status;
        }
    }
    
    return 0;
}

static int set_input_mode(void)
{
    for (int i=0; i<DB_PINS_NUMBER; i++)
    {
        int change_direction_status;
        change_direction_status = gpio_direction_input(pin_db[i]);
        if (change_direction_status != 0)
        {
            printk(KERN_ERR "[KS0108] Cannot set pin %d as input\n", pin_db[i]);
            return change_direction_status;
        }
    }
    
    return 0;
}

static bool bit_read(u8 data, u8 bit_number)
{
    return (data>>bit_number)&1;
}

static void write_to_db(u8 db_data)
{
    for (int i=0; i<DB_PINS_NUMBER; i++)
    {
        bool pin_value = bit_read(db_data, i);
        gpio_set_value(pin_db[i], pin_value);
    }
    
    udelay(1);
}

static u8 get_db_data(void)
{
    u8 data = 0;
    
    for (int i=0; i<DB_PINS_NUMBER; i++)
    {
        data = data | (gpio_get_value(pin_db[i]) << i);
    }
    
    return data;
}

static bool lcd_is_busy(u8 data)
{
    return data >> 7;
}

static u8 exec_lcd_cmd(command_t *cmd)
{
    gpio_set_value(pin_rs, cmd->rs);
    gpio_set_value(pin_rw, cmd->rw);
    
    if (cmd->rw == 0)
    {
        set_output_mode();
        write_to_db(cmd->db);
        gpio_set_value(pin_en, 1);
        udelay(1);
        gpio_set_value(pin_en, 0);
        return 0;
    }
    
    else
    {
        set_input_mode();
        gpio_set_value(pin_en, 1);
        udelay(1);
        u8 data = get_db_data();
        gpio_set_value(pin_en, 0);
        return data;
    }
}

static void display_turn(bool enable)
{
    command_t cmd = {
        .rs = 0,
        .rw = 0,
        .db = 0b00111110 | enable
    };
    exec_lcd_cmd(&cmd); 
}

static void set_addr_y(u8 y) {
  command_t cmd = {
    .rs = 0,
    .rw = 0,
    .db = 0b01000000 | y
  };
  exec_lcd_cmd(&cmd);
}

static void set_page_x(u8 x) {
  command_t cmd = {
    .rs = 0,
    .rw = 0,
    .db = 0b10111000 | x
  };
  exec_lcd_cmd(&cmd);
}

static void write_display_data(u8 data) {
  command_t cmd = {
    .rs = 1,
    .rw = 0,
    .db = data
  };
  exec_lcd_cmd(&cmd);
}

static u8 status_read(void) {
  command_t cmd = {
    .rs = 0,
    .rw = 1,
    .db = 0
  };

  return exec_lcd_cmd(&cmd);
}


static void write_screen_data(u8 data[]) {
  int data_pos = 0;

  for (int page_x = 0; page_x<8; page_x++) 
  {
    select_cs(pin_cs1);  
    set_addr_y(0);
    set_page_x(page_x);
    int i;
    for (i=0; i<64; i++) {
        if (sync_mode_en) wait_for_lcd();
        write_display_data(data[i+data_pos]);
    }
    data_pos += i;

    select_cs(pin_cs2);
    set_addr_y(0);
    set_page_x(page_x);
    for (i=0; i<64; i++) {
        if (sync_mode_en) wait_for_lcd();
        write_display_data(data[i+data_pos]);
    }
    data_pos += i;
  }
}


static int init_lcd(void)
{
    int select_cs_result = select_cs(pin_cs1);
    if (select_cs_result != 0) return select_cs_result;
    display_turn(true);
    select_cs_result = select_cs(pin_cs2);
    if (select_cs_result != 0) return select_cs_result;
    display_turn(true);
    write_screen_data(framebuffer);
    return 0;
}

static int __init ks0108_init(void)
{
    printk(KERN_INFO "[KS0108] Initializing the KS0108 driver\n");
    int init_gpios_result;
    init_gpios_result = init_gpios();
    if (init_gpios_result != 0) return init_gpios_result;
    int init_device_result;
    init_device_result = init_device();
    if (init_device_result != 0) return init_device_result;
    int init_lcd_result;
    init_lcd_result = init_lcd();
    if (init_lcd_result != 0) return init_lcd_result;
    return 0;
}

static void __exit ks0108_exit(void)
{
    uninit(INIT_STEP_END);
    printk(KERN_INFO "KS0108 driver has been disabled\n");
}

module_init(ks0108_init);
module_exit(ks0108_exit);
