obj-m+=ks0108.o

ARCH=arm
CROSS_COMPILE=arm-linux-gnueabihf-
KERNEL_DIR=/home/user/rabarbar/linux

ccflags-y := -std=gnu99 -Wno-declaration-after-statement

all:
	make -C $(KERNEL_DIR) M=$(PWD) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) modules
clean:
	make -C $(KERNEL_DIR) M=$(PWD) clean
